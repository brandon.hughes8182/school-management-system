const { app } = require('./src/app');

const PORT = process.argv[2] || 3000;

app.listen(PORT, () => console.log(`App listening on port ${PORT}.`));
