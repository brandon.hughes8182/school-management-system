const supertest = require('supertest');
const { app } = require('../src/app');

const request = supertest(app);

describe('Students', function () {
  describe('GET /students/:studentId', function () {
    it('returns an error if the student does not exist', function () {
      const expected = {
        errors: ['Student with ID 999 does not exist.']
      };

      return request.get('/students/999').expect(400, expected);
    });
  });

  describe('POST /students', function () {
    it('inserts a new student record', function () {
      const data = {
        first_name: 'Foo',
        last_name: 'Bar',
        birth_date: '2022-01-15',
        enrolled_date: '2022-02-22',
      };

      const expected = { id: 61, ...data };

      return request
        .post('/students')
        .send(data)
        .expect(201, expected);
    });
  });

  describe('PUT /students/:studentId', function () {
    it('updates an existing course record', function () {
      const data = {
        first_name: 'Foo',
        last_name: 'Bar',
        birth_date: '2022-01-15',
        enrolled_date: '2022-02-22',
      };

      const expected = { id: 1, ...data };

      return request
        .put('/students/1')
        .send(data)
        .expect(200, expected);
    });
  });

  describe('DELETE /student/:studentId', function () {
    it('deletes an existing course record', function () {
      return request
        .delete('/students/1')
        .expect(200);
    });
  });
});
