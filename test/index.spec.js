const { db } = require('../src/db');

before(async function () {
  await db.migrate.latest({ directory: './src/db/migrations' });
});

after(async function () {
  await db.destroy();
});

beforeEach(async function () {
  await db.seed.run();
});


