const swaggerJsDoc = require('swagger-jsdoc');

const swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: 'School Management System API',
    version: '1.0.0',
  },
  tags: [
    { name: 'courses', description: 'Interact with course records.' },
    { name: 'enrollments', description: 'Interact with student course enrollments.' },
    { name: 'gradebook', description: 'Interact with student gradebook records.' },
    { name: 'instructors', description: 'Interact with instructor records.' },
    { name: 'students', description: 'Interact with student records.' },
  ],
  components: {
    schemas: {
      ApiError: {
        type: 'object',
        properties: {
          errors: {
            type: 'array',
            items: {
              type: 'string',
            },
          },
        },
      },
    },
  },
};

const swaggerSpec = swaggerJsDoc({ swaggerDefinition, apis: ['./src/routers/*.router.js'] });

module.exports = { swaggerSpec };