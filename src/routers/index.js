const courseRouter = require('./course.router');
const enrollmentRouter = require('./enrollment.router');
const gradebookRouter = require('./gradebook.router');
const instructorRouter = require('./instructor.router');
const studentRouter = require('./student.router');

module.exports = {
  courseRouter,
  enrollmentRouter,
  gradebookRouter,
  instructorRouter,
  studentRouter,
};
