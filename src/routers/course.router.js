const express = require('express');
const { courseMiddleware, parseQueryParams } = require('../middleware');
const { courseController } = require('../controllers');

const courseRouter = express.Router();

/**
 * @swagger
 * components:
 *   schemas:
 *     NewCourse:
 *       type: object
 *       properties:
 *         identifier:
 *           type: string
 *           example: 'CS492-2104A-01'
 *         name:
 *           type: string
 *           example: Computer Science Group Project II
 *         description:
 *           type: string
 *           example: Capstone course for the BSCS program.
 *         start_date:
 *           type: string
 *           description: The first day of the course, formatted as YYYY-MM-DD.
 *           example: '2021-11-03'
 *         end_date:
 *           type: string
 *           description: The last day of the course, formatted as YYYY-MM-DD.
 *           example: '2022-12-15'
 *         capacity:
 *           type: integer
 *           description: The maximum number of students that can be enrolled in the course.
 *           example: 20
 *     Course:
 *       allOf:
 *         - type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: An ineteger ID that is unique to this course record.
 *               example: 14
 *         - $ref: '#/components/schemas/NewCourse'
 *         - type: object
 *           properties:
 *             enrolled:
 *               type: integer
 *               description: The number of students currently enrolled in the course.
 *               example: 13
 */

/**
 * @swagger
 * /courses:
 *   get:
 *     tags:
 *       - courses
 *     summary: Retrieves all course records.
 *     responses:
 *       200:
 *         description: A list of all courses.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Course'
 */
courseRouter.get('/', courseController.getCourses);

/**
 * @swagger
 * /courses/{courseId}:
 *   get:
 *     tags:
 *       - courses
 *     summary: Retrieves a single course.
 *     parameters:
 *       - in: path
 *         name: courseId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The integer ID of the course record to retrieve.
 *     responses:
 *       200:
 *         description: A course record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Course'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
courseRouter.get(
  '/:courseId',
  parseQueryParams,
  courseMiddleware.validateGetCourseByIdReq,
  courseController.getCourseById
);

/**
 * @swagger
 * /courses:
 *   post:
 *     tags:
 *       - courses
 *     summary: Creates a new course record.
 *     requestBody:
 *       description: The information to create a new course record.
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewCourse'
 *     responses:
 *       201:
 *         description: The new course record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Course'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
courseRouter.post(
  '/',
  parseQueryParams,
  courseMiddleware.validateCreateCourseReq,
  courseController.createCourse
);

/**
 * @swagger
 * /courses/{courseId}:
 *   put:
 *     tags:
 *       - courses
 *     summary: Updates an existing course record.
 *     parameters:
 *       - in: path
 *         name: courseId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The integer ID of the course record to be updated.
 *     requestBody:
 *       description: The information to update the course record.
 *       required: true,
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewCourse'
 *     responses:
 *       200:
 *         description: The updated course record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Course'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
courseRouter.put(
  '/:courseId',
  parseQueryParams,
  courseMiddleware.validateUpdateCourseReq,
  courseController.updateCourseById
);

/**
 * @swagger
 * /courses/{courseId}:
 *   delete:
 *     tags:
 *       - courses
 *     summary: Deletes an existing course record.
 *     parameters:
 *       - in: path
 *         name: courseId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The integer ID of the course record to be deleted.
 *     responses:
 *       200:
 *         description: Confirmation that the course record was successfully deleted.
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
courseRouter.delete(
  '/:courseId',
  parseQueryParams,
  courseMiddleware.validateDeleteCourseReq,
  courseController.deleteCourseById
);

module.exports = courseRouter;
