const express = require('express');
const { enrollmentMiddleware, parseQueryParams } = require('../middleware');
const { enrollmentController } = require('../controllers');

const enrollmentRouter = express.Router();

/**
 * @swagger
 * components:
 *   schemas:
 *     NewEnrollment:
 *       type: object
 *       properties:
 *         course_id:
 *           type: integer
 *           description: The integer ID of the course.
 *           example: 8
 *         student_id:
 *           type: integer
 *           description: The integer ID of the enrolled student.
 *           example: 35
 *     Enrollment:
 *       allOf:
 *         - type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: An integer ID unique to this enrollment record.
 *               example: 183
 *         - $ref: '#/components/schemas/NewEnrollment'
 */

/**
 * @swagger
 * /enrollments/{enrollmentId}:
 *   get:
 *     tags:
 *       - enrollments
 *     summary: Retrieves a single enrollment record.
 *     parameters:
 *       - in: path
 *         name: enrollmentId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The integer ID of the enrollment record to retrieve.
 *     responses:
 *       200:
 *         description: The enrollment record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Enrollment'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
enrollmentRouter.get(
  '/:enrollmentId',
  parseQueryParams,
  enrollmentMiddleware.validateGetEnrollmentByIdReq,
  enrollmentController.getEnrollmentById
);

/**
 * @swagger
 * /enrollments:
 *   post:
 *     tags:
 *       - enrollments
 *     summary: Enrolls a student into a course.
 *     description: >
 *       Enrolls a student into a course if the course has not yet reached capacity.
 *       If the course is at capacity, enrollment will be restricted.
 *     requestBody:
 *       description: The information to create a new enrollment record.
 *       required: true,
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewEnrollment'
 *     responses:
 *       201:
 *         description: The new enrollment record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Enrollment'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
enrollmentRouter.post(
  '/',
  parseQueryParams,
  enrollmentMiddleware.validateCreateEnrollmentReq,
  enrollmentController.createEnrollment
);

module.exports = enrollmentRouter;