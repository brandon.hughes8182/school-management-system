const express = require('express');
const { courseMiddleware, gradebookMiddleware, studentMiddleware, parseQueryParams } = require('../middleware');
const { studentController, gradebookController, courseController } = require('../controllers');

const studentRouter = express.Router();

/**
 * @swagger
 * components:
 *   schemas:
 *     NewStudent:
 *       type: object
 *       properties:
 *         first_name:
 *           type: string
 *           example: 'Alice'
 *         last_name:
 *           type: string
 *           example: 'Smith'
 *         birth_date:
 *           type: string
 *           description: The student's birth date, formatted as YYYY-MM-DD.
 *           example: '1970-05-20'
 *         enrolled_date:
 *           type: string
 *           description: The date when the student completed the enrollment process, formatted as YYYY-MM-DD.
 *           example: '2019-08-13'
 *     Student:
 *       allOf:
 *         - type: object
 *           properties:
 *             id:
 *               type: integer
 *               description: An ineteger ID unique to this student record.
 *               example: 42
 *         - $ref: '#/components/schemas/NewStudent'
 */

/**
 * @swagger
 * /students:
 *   get:
 *     tags:
 *       - students
 *     summary: Retrieves all student records.
 *     responses:
 *       200:
 *         description: A list of all enrolled students.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Student'
 */
studentRouter.get('/', studentController.getStudents);

/**
 * @swagger
 * /students/{studentId}:
 *   get:
 *     tags:
 *       - students
 *     summary: Retrieves a single student record.
 *     parameters:
 *       - in: path
 *         name: studentId
 *         schema:
 *           type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: The studen record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Student'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
studentRouter.get(
  '/:studentId',
  parseQueryParams,
  studentMiddleware.validateGetStudentByIdReq,
  studentController.getStudentById
);

/**
 * @swagger
 * /students:
 *   post:
 *     tags:
 *       - students
 *     summary: Creates a new student record.
 *     requestBody:
 *       description: The information to create a new student record.
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewStudent'
 *     responses:
 *       201:
 *         description: The new student record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Student'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
studentRouter.post(
  '/',
  parseQueryParams,
  studentMiddleware.validateCreateStudentReq,
  studentController.createStudent
);

/**
 * @swagger
 * /students/{studentId}:
 *   put:
 *     tags:
 *       - students
 *     summary: Updates an existing student record.
 *     parameters:
 *       - in: path
 *         name: studentId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The integer ID of the student record to be updated.
 *     requestBody:
 *       description: The information to update the student record.
 *       required: true,
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewStudent'
 *     responses:
 *       200:
 *         description: The updated student record.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Student'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
studentRouter.put(
  '/:studentId',
  parseQueryParams,
  studentMiddleware.validateUpdateStudentReq,
  studentController.updateStudentById
);

/**
 * @swagger
 * /students/{studentId}:
 *   delete:
 *     tags:
 *       - students
 *     summary: Deletes an existing student record.
 *     parameters:
 *       - in: path
 *         name: studentId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The integer ID of the student record to be deleted.
 *     responses:
 *       200:
 *         description: Confirmation that the student record was successfully deleted.
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
studentRouter.delete(
  '/:studentId',
  parseQueryParams,
  studentMiddleware.validateDeleteStudentReq,
  studentController.deleteStudentById
);

/**
 * @swagger
 * /students/{studentId}/courses:
 *   get:
 *     tags:
 *       - courses
 *     summary: Retrieves the course records for a single student.
 *     parameters:
 *       - in: path
 *         name: studentId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The ID of the student to retrieve course records for.
 *     responses:
 *       200:
 *         description: A list of course records for the student.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Course'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
studentRouter.get(
  '/:studentId/courses',
  parseQueryParams,
  courseMiddleware.validateGetCoursesByStudentIdReq,
  courseController.getCoursesByStudentId
);

/**
 * @swagger
 * /students/{studentId}/gradebook:
 *   get:
 *     tags:
 *       - gradebook
 *     summary: Retrieves the gradebook records for a single student.
 *     parameters:
 *       - in: path
 *         name: studentId
 *         schema:
 *           type: integer
 *         required: true
 *         description: The ID of the student to retrieve gradebook records for.
 *     responses:
 *       200:
 *         description: A list of gradebook records for the student.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Grade'
 *       400:
 *         description: Invalid request.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiError'
 */
studentRouter.get(
  '/:studentId/gradebook',
  parseQueryParams,
  gradebookMiddleware.validateGetGradesReq,
  gradebookController.getGradesByStudentId
);

module.exports = studentRouter;
