const { db } = require('../db');
const { sqlTimestamp } = require('../utils');

async function createEnrollment(req, res) {
  const { course_id, student_id } = req.body;

  const errors = [];
  const course = await db('courses').select('id', 'capacity').where({ id: course_id }).first();
  const student = await db('students').select('id').where({ id: student_id }).first();

  if (!course) errors.push(`Course with ID ${course_id} does not exist.`);
  if (!student) errors.push(`Student with ID ${student_id} does not exist.`);
  if (errors.length) return res.status(400).json({ errors });

  const query = `
    SELECT
      SUM(CASE WHEN course_id = ${course_id} THEN 1 ELSE 0 END) AS totalEnrolled,
      SUM(CASE WHEN student_id = ${student_id} AND course_id = ${course_id} THEN 1 ELSE 0 END) AS isAlreadyEnrolled
    FROM students_courses;
  `.trim();

  const { totalEnrolled, isAlreadyEnrolled } = (await db.raw(query))[0];

  if (totalEnrolled >= course.capacity) {
    return res.status(400).json({
      errors: [`Course with ID ${course_id} is at capacity (${course.capacity}).`]
    });
  }

  if (isAlreadyEnrolled) {
    return res.status(400).json({
      errors: [`Student with ID ${student_id} is already enrolled in course with ID ${course_id}`]
    });
  }

  const ts = sqlTimestamp();
  const [newId] = await db('students_courses')
    .insert({ student_id: student_id, course_id: course_id, created_at: ts, updated_at: ts });

  return res.status(201).json({ id: newId, student_id: student_id, course_id: course_id });
}

async function getEnrollmentById(req, res) {
  const { enrollmentId } = req.params;
  const result = await db('students_courses')
    .where({ id: enrollmentId })
    .select('id', 'student_id', 'course_id')
    .first();

  if (!result)
    return res.status(400).json({ errors: [`Enrollment with ID ${enrollmentId} does not exist.`] });
  return res.status(200).json(result);
}

module.exports = {
  createEnrollment,
  getEnrollmentById,
};
