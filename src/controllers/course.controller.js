const { db } = require('../db');
const { sqlTimestamp } = require('../utils');

async function getCourses(_, res) {
  const courses = await db('courses as c')
    .join('students_courses as sc', 'c.id', 'sc.course_id')
    .select('c.id', 'c.identifier', 'c.name', 'c.description', 'c.start_date', 'c.end_date', 'c.capacity')
    .count('* as enrolled')
    .groupBy('c.id');

  return res.status(200).json(courses);
}

async function getCoursesByStudentId(req, res) {
  const { studentId } = req.params;

  const courses = await db('courses as c')
    .join('students_courses as sc', 'c.id', 'sc.course_id')
    .join('students_courses as sc2', 'c.id', 'sc.course_id')
    .join('students as s', 's.id', 'sc.student_id')
    .where('s.id', studentId)
    .select(
      'c.id',
      'c.identifier',
      'c.name',
      'c.description',
      'c.start_date',
      'c.end_date',
      'c.capacity',
      db.raw('sum(case when c.id = sc2.course_id then 1 else 0 end) as enrolled')
    )
    .groupBy('c.id');

  return res.status(200).json(courses);
}

async function getCourseById(req, res) {
  const { courseId } = req.params;

  const course = await db('courses')
    .where({ id: courseId })
    .select('id', 'identifier', 'name', 'description', 'start_date', 'end_date', 'capacity')
    .first();

  if (!course)
    return res.status(400).json({ errors: [`Course with ID ${courseId} does not exist.`] });

  const { enrolled } = await db('students_courses')
    .where({ course_id: courseId })
    .count('* as enrolled')
    .first();

  return res.status(200).json({ ...course, enrolled });
}

async function createCourse(req, res) {
  const ts = sqlTimestamp();
  const [newId] = await db('courses')
    .insert({ ...req.body, created_at: ts, updated_at: ts, });

  return res.status(201).json({ id: newId, ...req.body });
}

async function updateCourseById(req, res) {
  const { courseId } = req.params;

  const ts = sqlTimestamp();
  await db('courses').where({ id: courseId }).update({ ...req.body, updated_at: ts });

  return res.status(200).json({ id: courseId, ...req.body });
}

async function deleteCourseById(req, res) {
  const { courseId } = req.params;
  await db('courses').where({ id: courseId }).delete();
  res.sendStatus(200);
}

module.exports = {
  getCourses,
  getCoursesByStudentId,
  getCourseById,
  createCourse,
  updateCourseById,
  deleteCourseById,
};
