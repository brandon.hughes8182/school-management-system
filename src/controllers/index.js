const courseController = require('./course.controller');
const enrollmentController = require('./enrollment.controller');
const gradebookController = require('./gradebook.controller');
const instructorController = require('./instructor.controller');
const studentController = require('./student.controller');

module.exports = {
  courseController,
  enrollmentController,
  gradebookController,
  instructorController,
  studentController,
};
