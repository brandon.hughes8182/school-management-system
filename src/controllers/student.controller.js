const { db } = require('../db');
const { sqlTimestamp } = require('../utils');

async function getStudents(_, res) {
  const students = await db('students')
    .select('id', 'first_name', 'last_name', 'birth_date', 'enrolled_date');
  return res.status(200).json(students);
}

async function getStudentById(req, res) {
  const { studentId } = req.params;
  const result = await db('students')
    .where({ id: studentId })
    .select('id', 'first_name', 'last_name', 'birth_date', 'enrolled_date')
    .first();

  if (!result)
    return res.status(400).json({ errors: [`Student with ID ${studentId} does not exist.`] });
  return res.status(200).json(result);
}

async function createStudent(req, res) {
  const ts = sqlTimestamp();
  const [newId] = await db('students')
    .insert({ ...req.body, created_at: ts, updated_at: ts });

  return res.status(201).json({ id: newId, ...req.body });
}

async function updateStudentById(req, res) {
  const { studentId } = req.params;

  const ts = sqlTimestamp();
  await db('students').where({ id: studentId }).update({ ...req.body, updated_at: ts });

  return res.status(200).json({ id: studentId, ...req.body });
}

async function deleteStudentById(req, res) {
  const { studentId } = req.params;
  await db('students').where({ id: studentId }).delete();
  res.sendStatus(200);
}

module.exports = {
  getStudents,
  getStudentById,
  createStudent,
  updateStudentById,
  deleteStudentById,
};
