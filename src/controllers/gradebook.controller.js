const { db } = require('../db');
const { sqlTimestamp } = require('../utils');

async function getGradesByStudentId(req, res) {
  const { studentId } = req.params;
  const result = await db('grades')
    .where({ student_id: studentId })
    .select(
      'id',
      'student_id',
      'course_id',
      'assignment_name',
      'assignment_due_date',
      'points_possible',
      'points_earned'
    );

  return res.status(200).json(result);
}

async function getGradeById(req, res) {
  const { gradeId } = req.params;
  const result = await db('grades')
    .where({ id: gradeId })
    .select(
      'id',
      'student_id',
      'course_id',
      'assignment_name',
      'assignment_due_date',
      'points_possible',
      'points_earned'
    )
    .first();

  if (!result)
    return res.status(400).json({ errors: [`Grade with ID ${gradeId} does not exist.`] });
  return res.status(200).json(result);
}

async function createGrade(req, res) {
  const { student_id, course_id, assignment_name } = req.body;

  const errors = [];
  const course = await db('courses').select('id').where({ id: course_id }).first();
  const student = await db('students').select('id').where({ id: student_id }).first();

  if (!course) errors.push(`Course with ID ${course_id} does not exist.`);
  if (!student) errors.push(`Student with ID ${student_id} does not exist.`);
  if (errors.length) return res.status(400).json({ errors });

  const existingGrade = await db('grades')
    .select('id')
    .where({ student_id, assignment_name })
    .first();

  if (existingGrade) {
    return res.status(400).json({
      errors: [`Student with ID ${student_id} already has a grade for assignment: ${assignment_name}`]
    });
  }

  const ts = sqlTimestamp();
  const [newId] = await db('grades')
    .insert({ ...req.body, created_at: ts, updated_at: ts });

  return res.status(201).json({ id: newId, ...req.body });
}

async function updateGradeById(req, res) {
  const { gradeId } = req.params;

  const ts = sqlTimestamp();
  await db('grades').where({ id: gradeId }).update({ ...req.body, updated_at: ts });

  return res.status(200).json({ id: gradeId, ...req.body });
}

async function deleteGradeById(req, res) {
  const { gradeId } = req.params;
  await db('grades').where({ id: gradeId }).delete();
  return res.sendStatus(200);
}

module.exports = {
  getGradesByStudentId,
  getGradeById,
  createGrade,
  updateGradeById,
  deleteGradeById,
};
