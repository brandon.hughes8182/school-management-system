const studentCourseData = require('./seed-data/students-courses.json');
const { sqlTimestamp } = require('../../utils');

exports.seed = async function (knex) {
  await knex('students_courses').del();
  await knex('sqlite_sequence').delete().where({ name: 'students_courses' });

  const ts = sqlTimestamp();
  await knex('students_courses')
    .insert(studentCourseData.map(e => ({ ...e, created_at: ts, updated_at: ts })));
};
