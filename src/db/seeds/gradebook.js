const courseData = require('./seed-data/courses.json');
const studentCourseData = require('./seed-data/students-courses.json');
const { sqlTimestamp } = require('../../utils');

function generateGrades(studentId, courseId, startDate) {
  let entries = [];

  for (let i = 1; i <= 4; i++) {
    const date = new Date(startDate);
    date.setDate(date.getDate() + 7 * i);

    entries.push({
      student_id: studentId,
      course_id: courseId,
      assignment_name: `Week ${i} Discussion Board`,
      assignment_due_date: date.toISOString().slice(0, 10),
      points_possible: 100,
      points_earned: Math.floor(Math.random() * (100 - 60) + 60),
    });

    entries.push({
      student_id: studentId,
      course_id: courseId,
      assignment_name: `Week ${i} Individual Project`,
      assignment_due_date: date.toISOString().slice(0, 10),
      points_possible: 200,
      points_earned: Math.floor(Math.random() * (200 - 150) + 150),
    });
  }

  return entries;
}

exports.seed = async function (knex) {
  await knex('grades').del();
  await knex('sqlite_sequence').delete().where({ name: 'grades' });

  const startDates = courseData.map(v => v.start_date);

  const combined = studentCourseData.map(studentCourse => {
    return { ...studentCourse, start_date: startDates[studentCourse.course_id - 1] };
  });

  const grades = combined.flatMap(({ student_id, course_id, start_date }) => {
    return generateGrades(student_id, course_id, start_date);
  });

  const ts = sqlTimestamp();
  const withTimestamps = grades.map(grade => ({ ...grade, created_at: ts, updated_at: ts }));

  await knex('grades').insert(withTimestamps.slice(0, 250));
  await knex('grades').insert(withTimestamps.slice(250));
};
