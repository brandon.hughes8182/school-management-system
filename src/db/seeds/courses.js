const courseData = require('./seed-data/courses.json');
const { sqlTimestamp } = require('../../utils');

exports.seed = async function (knex) {
  await knex('courses').del();
  await knex('sqlite_sequence').delete().where({ name: 'courses' });

  const ts = sqlTimestamp();
  await knex('courses')
    .insert(courseData.map(d => ({ ...d, created_at: ts, updated_at: ts })));
};
