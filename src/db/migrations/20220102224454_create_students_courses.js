exports.up = function (knex) {
  return knex.schema.createTable('students_courses', function (table) {
    table.increments('id');
    table
      .integer('student_id')
      .notNullable()
      .references('id')
      .inTable('students')
      .onDelete('CASCADE');
    table
      .integer('course_id')
      .notNullable()
      .references('id')
      .inTable('courses')
      .onDelete('CASCADE');
    table.timestamps();
  });

};

exports.down = function (knex) {
  return knex.schema.dropTable('students_courses');
};
