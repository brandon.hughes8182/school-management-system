exports.up = function (knex) {
  return knex.schema.createTable('grades', function (table) {
    table.increments('id');
    table
      .integer('student_id')
      .notNullable()
      .references('id')
      .inTable('students')
      .onDelete('CASCADE');
    table
      .integer('course_id')
      .notNullable()
      .references('id')
      .inTable('courses')
      .onDelete('CASCADE');
    table.string('assignment_name', 255).notNullable();
    table.date('assignment_due_date').notNullable();
    table.integer('points_possible').notNullable();
    table.integer('points_earned').notNullable();
    table.timestamps();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable('grades');
};
