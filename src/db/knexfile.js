const path = require('path');

module.exports = {
  development: {
    client: 'sqlite3',
    connection: { filename: path.join(__dirname, 'db.sqlite3') },
    migrations: { tableName: 'knex_migrations' },
    seeds: { directory: path.join(__dirname, 'seeds') },
  },
  test: {
    client: 'sqlite3',
    connection: ':memory:',
    migrations: { directory: path.join(__dirname, 'migrations') },
    seeds: { directory: path.join(__dirname, 'seeds') },
  }
};
