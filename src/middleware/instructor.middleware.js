const { validate, makeParamsValidation } = require('./utils');

/** @type { import('./utils').JSONSchema7 } */
const instructorDtoSchema = {
  type: 'object',
  required: ['first_name', 'last_name', 'birth_date', 'hired_date'],
  properties: {
    first_name: { type: 'string', minLength: 1, maxLength: 255, pattern: "^[\\sa-zA-Z'-]*$" },
    last_name: { type: 'string', minLength: 1, maxLength: 255, pattern: "^[\\sa-zA-Z'-]*$" },
    birth_date: { type: 'string', format: 'date' },
    hired_date: { type: 'string', format: 'date' },
  },
  additionalProperties: false,
};

const validateGetInstructorByIdReq = validate({ params: makeParamsValidation('instructorId') });

const validateCreateInstructorReq = validate({ body: instructorDtoSchema });

const validateUpdateInstructorReq = validate({
  params: makeParamsValidation('instructorId'),
  body: instructorDtoSchema,
});

const validateDeleteInstructorReq = validate({ params: makeParamsValidation('instructorId') });

module.exports = {
  validateGetInstructorByIdReq,
  validateCreateInstructorReq,
  validateUpdateInstructorReq,
  validateDeleteInstructorReq,
};
