const { validate, makeParamsValidation } = require('./utils');

/** @type { import('./utils').JSONSchema7 } */
const courseDtoSchema = {
  type: 'object',
  required: ['identifier', 'name', 'description', 'start_date', 'end_date', 'capacity'],
  properties: {
    identifier: {
      type: 'string',
      minLength: 1,
      maxLength: 255,
    },
    name: {
      type: 'string',
      minLength: 1,
      maxLength: 255,
    },
    description: {
      type: 'string'
    },
    start_date: {
      type: 'string',
      format: 'date',
    },
    end_date: {
      type: 'string',
      format: 'date',
    },
    capacity: {
      type: 'number',
      multipleOf: 1,
    }
  },
  additionalProperties: false,
};

const validateGetCoursesByStudentIdReq = validate({ params: makeParamsValidation('studentId') });

const validateGetCourseByIdReq = validate({ params: makeParamsValidation('courseId') });

const validateCreateCourseReq = validate({ body: courseDtoSchema });

const validateUpdateCourseReq = validate({
  params: makeParamsValidation('courseId'),
  body: courseDtoSchema,
});

const validateDeleteCourseReq = validate({ params: makeParamsValidation('courseId') });

module.exports = {
  validateGetCoursesByStudentIdReq,
  validateGetCourseByIdReq,
  validateCreateCourseReq,
  validateUpdateCourseReq,
  validateDeleteCourseReq,
};
