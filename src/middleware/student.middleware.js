const { validate, makeParamsValidation } = require('./utils');

/** @type { import('./utils').JSONSchema7 } */
const userDtoSchema = {
  type: 'object',
  required: ['first_name', 'last_name', 'birth_date', 'enrolled_date'],
  properties: {
    first_name: {
      type: 'string',
      minLength: 1,
      maxLength: 255,
      pattern: "^[\\sa-zA-Z'-]*$",
    },
    last_name: {
      type: 'string',
      minLength: 1,
      maxLength: 255,
      pattern: "^[\\sa-zA-Z'-]*$",
    },
    birth_date: {
      type: 'string',
      format: 'date',
    },
    enrolled_date: {
      type: 'string',
      format: 'date',
    },
  },
  additionalProperties: false,
};

const validateGetStudentByIdReq = validate({ params: makeParamsValidation('studentId') });

const validateCreateStudentReq = validate({ body: userDtoSchema });

const validateUpdateStudentReq = validate({
  params: makeParamsValidation('studentId'),
  body: userDtoSchema,
});

const validateDeleteStudentReq = validate({ params: makeParamsValidation('studentId') });

module.exports = {
  validateGetStudentByIdReq,
  validateCreateStudentReq,
  validateUpdateStudentReq,
  validateDeleteStudentReq,
};
