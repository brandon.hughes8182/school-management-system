function parseQueryParams(req, _, next) {
  req.params = Object.fromEntries(
    Object.entries(req.params).map(([k, v]) => [k, Number.parseInt(v)])
  );
  next();
}

module.exports = { parseQueryParams };