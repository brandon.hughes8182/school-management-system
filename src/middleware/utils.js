const { Validator } = require('express-json-validator-middleware');
const { validate } = new Validator({ allErrors: true });

/**
 * @typedef { import('../../node_modules/@types/json-schema').JSONSchema7 } JSONSchema7
 */

/** @returns { JSONSchema7 } */
function makeParamsValidation(...paramNames) {
  return {
    type: 'object',
    required: paramNames,
    properties: {
      ...paramNames.reduce((acc, p) => {
        return { ...acc, [p]: { type: 'number', multipleOf: 1 } };
      }, {})
    }
  };
}

module.exports = {
  validate,
  makeParamsValidation,
};
